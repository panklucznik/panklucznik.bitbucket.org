/**
 * -prefix-free plugin for rudimentary CSS variables support
 * @author Lea Verou
 */

(function() {

if(!window.StyleFix || !window.PrefixFree) {
	return;
}

var prefix = PrefixFree.prefix, dummy = document.createElement('_').style;

dummy.cssText = '--foo: red; background: var(--foo);';

if (dummy.background) {
	return;
}

dummy.cssText = prefix + '--foo: red; background: ' + prefix + 'var(--foo);';

var vars = {};

StyleFix.register(function(css) {
	return css.replace(/(?:^|\{|\s|;)--(?:[\w-]+)\s*:\s*[^;}]+|(\s|:|,)var\s*\(--([\w-]+)\)/gi, function($0, before, id) {
		var declaration = $0.match(/(^|\{|\s|;)--([\w-]+)\s*:\s*([^;}]+)/i);
		
		if (declaration) {
			vars[declaration[2]] = declaration[3];
		}
		else {
			return before + (vars[id] || 'initial');
		}
	});
});

var vars = {};

StyleFix.register(function(css) {
	return css.replace(/(?:^|\{|\s|;)--(?:[\w-]+)\s*:\s*[^;}]+|(\s|:|,)var\s*\(--([\w-]+)\)/gi, function($0, before, id) {
		var declaration = $0.match(/(^|\{|\s|;)--([\w-]+)\s*:\s*([^;}]+)/i);
		
		if (declaration) {
			vars[declaration[2]] = declaration[3];
		}
		else {
			return before + (vars[id] || 'initial');
		}
	});
});

StyleFix.register(function(css) {
	return css.replace(/(?:^|\{|\s|;)--(?:[\w-]+)\s*:\s*[^;}]+|(\s|:|,)var\s*\(--([\w-]+)\)/gi, function($0, before, id) {
		var declaration = $0.match(/(^|\{|\s|;)--([\w-]+)\s*:\s*([^;}]+)/i);
		
		if (declaration) {
			vars[declaration[2]] = declaration[3];
		}
		else {
			return before + (vars[id] || 'initial');
		}
	});
});



})();
