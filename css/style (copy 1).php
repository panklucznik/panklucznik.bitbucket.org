<?php
    header("Content-type: text/css; charset: CHARSET=iso-8859-2");
?>

<?php
    header("Content-type: text/css; charset: CHARSET=iso-8859-2");

	$border = "2vmin";
	$border_type = "solid";
	
	$font_m = "3mm";
	$font_t = "4mm";
	$font_d = "5mm";
	
	$max_w = "1400px";
	
	$persp = "30cm";
	
	$radius = "5vmin";
	
	$frame_w1 = "0.0vmin";
	$frame_w2 = "0.0vmin";
	$frame_w3 = "0.0vmin";
	$frame_w4 = "0.0vmin";
	$frame_logo = "0";
	
	$c1_hue = "150";
	$c2_hue = "180";
	$c3_hue = "200";
	$c4_hue = "220";

	$c_sat = "60%";
	
	$c_l_text = "15%";
	$c_l_bg = "85%";
	$c_l_border = "30%";
	
	$c1_sat = $c_sat;
	$c2_sat = $c_sat;
	$c3_sat = $c_sat;
	$c4_sat = $c_sat;
	
	$alpha = "1";
	
	$icons = "none";
	
	$anim_trans_time_m = "0.2s";
	$anim_trans_time_t = "0.3s";
	$anim_trans_time_d = "0.5s";
	
	$c1_t = "hsla({$c1_hue}, {$c1_sat}, {$c_l_text}, {$alpha})";
	$c1_bg = "hsla({$c1_hue}, {$c1_sat}, {$c_l_bg}, {$alpha})";
	$c1_bg_s = "hsla({$c1_hue}, {$c1_sat}, {$c_l_bg}, 1)";
	$c1_b = "hsla({$c1_hue}, {$c1_sat}, {$c_l_border}, {$alpha})";
	
	$c2_t = "hsla({$c2_hue}, {$c2_sat}, {$c_l_text}, {$alpha})";
	$c2_bg = "hsla({$c2_hue}, {$c2_sat}, {$c_l_bg}, {$alpha})";
	$c2_bg_s = "hsla({$c2_hue}, {$c2_sat}, {$c_l_bg}, 1)";
	$c2_b = "hsla({$c2_hue}, {$c2_sat}, {$c_l_border}, {$alpha})";
	
	$c3_t = "hsla({$c3_hue}, {$c3_sat}, {$c_l_text}, {$alpha})";
	$c3_bg = "hsla({$c3_hue}, {$c3_sat}, {$c_l_bg}, {$alpha})";
	$c3_bg_s = "hsla({$c3_hue}, {$c3_sat}, {$c_l_bg}, 1)";
	$c3_b = "hsla({$c3_hue}, {$c3_sat}, {$c_l_border}, {$alpha})";
	
	$c4_t = "hsla($c4_hue, {$c4_sat}, {$c_l_text}, {$alpha})";
	$c4_bg = "hsla($c4_hue, {$c4_sat}, {$c_l_bg}, {$alpha})";
	$c4_bg_s = "hsla($c4_hue, {$c4_sat}, {$c_l_bg}, 1)";
	$c4_b = "hsla($c4_hue, {$c4_sat}, {$c_l_border}, {$alpha})";
	$logo_b = "black";
	
	$c0 = "hsla(0, 0, 0%, 0)";
?>

@font-face {
    font-family: 'FontAwesome2';
    src: url('../fonts/FontAwesome.eot');
    src: url('../fonts/FontAwesome.eot?#iefix') format('embedded-opentype'),
         url('../fonts/FontAwesome.woff') format('woff'),
         url('../fonts/FontAwesome.ttf') format('truetype');
    font-style: normal;
    font-weight: normal;
    text-rendering: optimizeLegibility;
}


[class*=""], [id*=""], body, h1, h2, h3, h4, p {
	display: inline-block;
}

.header, .menu li, #pasteContent, .aside, .footer, .logo img, .photo:active {
	box-shadow: 0 0.5vmin 1vmin rgba(0,0,0,0.12), 0 0.5vmin 1vmin rgba(0,0,0,0.24);
	border: <?php echo $border_type; ?>;
	border-radius: <?php echo $radius; ?>; 
	-o-border-radius: <?php echo $radius; ?>;
	-moz-border-radius: <?php echo $radius; ?>;
	-webkit-border-radius: <?php echo $radius; ?>;
	behavior: url(./ie/PIE.htc);
}

*, *:before, *:after {
	margin: 0;
	padding: 0;
	-webkit-box-sizing: inherit;
	-moz-box-sizing: inherit;
	-o-box-sizing: inherit;
	box-sizing: inherit;
	-webkit-transition: all <?php echo $anim_trans_time_m; ?>;
	-moz-transition: all <?php echo $anim_trans_time_m; ?>;
	-ms-transition: all <?php echo $anim_trans_time_m; ?>;
	-o-transition: all <?php echo $anim_trans_time_m; ?>;
	transition: all <?php echo $anim_trans_time_m; ?>;
	-webkit-font-smoothing: antialiased;
	text-rendering: optimizeLegibility;
	vertical-align: middle;
	-ms-interpolation-mode: bicubic;
	animation-duration: <?php echo $anim_trans_time_m; ?>;
	animation-delay: 0s;
	-o-animation-duration: <?php echo $anim_trans_time_m; ?>;
	-o-animation-delay: 0s;
	-moz-animation-duration: <?php echo $anim_trans_time_m; ?>;
	-moz-animation-delay: 0s;
	-webkit-animation-duration: <?php echo $anim_trans_time_m; ?>;
	-webkit-animation-delay: 0s;
}

html {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-o-box-sizing: border-box;
	box-sizing: border-box;
	font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
	font-size: <?php echo $font_m; ?>;
	background: transparent url(../img/bg.jpg) 0 0  repeat;
	max-width: <?php echo $max_w; ?>;
	margin: auto;
	perspective: <?php echo $persp; ?>;
	-o-perspective: <?php echo $persp; ?>;
	-moz-perspective: <?php echo $persp; ?>;
	-webkit-perspective: <?php echo $persp; ?>;
}

body {
	overflow: auto;
	margin: <?php echo $border; ?>;
}

h1 {
	font-size: 165%;
}

h2 {
	font-size: 140%;
}

h3 {
	font-size: 120%;
}

h4 {
	font-size: 110%;
}

p {
	font-size: 100%;
}

.row::after {
	content: "";
	clear: both;
	display: block;
}

[class*="col-"] {
	float: left;
	padding: <?php echo $border; ?>;
}

.header {
	padding: <?php echo $border; ?>;
	float: right;
	font-weight: bold;
	color: <?php echo $c1_t; ?>;
	border-color: <?php echo $c1_b; ?>;
	background-color: <?php echo $c1_bg; ?>;
	border-width: <?php echo $frame_w1; ?>;
}

.menu ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
}

.menu li {
	float: left;
	padding: 8px;
	margin-bottom: 7px;
	color: <?php echo $c2_t; ?>;
	border-color: <?php echo $c2_b; ?>;
	border-width: <?php echo $frame_w2; ?>;
	background-color: <?php echo $c2_bg; ?>;
}

.menu li:hover {
	background-color: <?php echo $c2_t; ?>;
	color: <?php echo $c2_bg; ?>;
}

.menu li:active {
	transform: translate(0, 20%);
	-o-transform: translate(0, 20%);
	-moz-transform: translate(0, 20%);
	-webkit-transform: translate(0, 20%);
}

.menu a {
	text-decoration: none;
}
	
#pasteContent {
	padding: <?php echo $border; ?>;
	color: <?php echo $c3_t; ?>;
	border-color: <?php echo $c3_b; ?>;
	border-width: <?php echo $frame_w3; ?>;
	background-color: <?php echo $c3_bg; ?>;
	max-width: 100%;
	overflow-x: hidden;
	overflow-y: hidden;
}

#pasteContent ol {
	list-style-type: decimal;
	margin: <?php echo $border; ?>;
	padding: 0;
	list-style-position: inside;
	overflow: hidden;
}

#pasteContent li {
	background: inherit;
	transform: inherit;
	-o-transform: inherit;
	-moz-transform: inherit;
	-webkit-transform: inherit;
}

#pasteContent a {
	text-decoration: none;
	color: inherit;
}

#pasteContent a::after {
	content: '\f0a5';
	font-family: FontAwesome, 'FontAwesome2';
	font-weight: normal;
	font-style: normal;
	margin:0px 0px 0px 10px;
	text-decoration:none;
}

#pasteContent a:hover {
	color: <?php echo $c3_bg; ?>;
	background-color: <?php echo $c3_t; ?>;
}

#pasteContent a:active {
	transform: translate(0.3vw, 0.3vh);
	-o-transform: translate(0.3vw, 0.3vh);
	-moz-transform: translate(0.3vw, 0.3vh);
	-webkit-transform: translate(0.3vw, 0.3vh);
}


.aside {
	padding: <?php echo $border; ?>;
	text-align: center;
	color: <?php echo $c4_t; ?>;
	border-color: <?php echo $c4_b; ?>;
	border-width: <?php echo $frame_w4; ?>;
	background-color: <?php echo $c4_bg; ?>;
	padding-right: 1px;
	overflow: hidden;
}

.footer {
	padding: <?php echo $border; ?>;
	text-align: center;
	color: <?php echo $c1_t; ?>;
	border-color: <?php echo $c1_b; ?>;
	border-width: <?php echo $frame_w1; ?>;
	background-color: <?php echo $c1_bg; ?>;
}

.logo {
	margin: <?php echo $border; ?>;
	margin-left: initial;
	float: left;
	display: block;
	width: 100%;
}

.logo img {
	border-color: <?php echo $logo_b; ?>;
	border-width: <?php echo $frame_logo; ?>;
	height: inherit;
	width: inherit;
	filter: inherit;
	display: inherit;
}

.logo img:active {
	transform: translate(10%, 10%) scale(0.8, 0.8);
	-o-transform: translate(10%, 10%) scale(0.8, 0.8);
	-moz-transform: translate(10%, 10%) scale(0.8, 0.8);
	-webkit-transform: translate(10%, 10%) scale(0.8, 0.8);
	box-shadow: none;
}

.test:active::after {
	content: " antoni.mitus@pwr.edu.pl";
}

.photo {
	float: right;
	max-width: 25%;
	margin-left: <?php echo $border; ?>;
	margin-right: <?php echo $border; ?>;
}

.photo:hover {
	filter: brightness(110%) contrast(110%);
	cursor: zoom-in;
}

.photo:active {
	cursor: zoom-out;
	filter: none;
	transform: scale(1.75, 1.75) translate(-15%, 20%);
	-o-transform: scale(1.75, 1.75) translate(-15%, 20%);
	-moz-transform: scale(1.75, 1.75) translate(-15%, 20%);
	-webkit-transform: scale(1.75, 1.75) translate(-15%, 20%);
}

.iframe-rwd  {
	position: relative;
	padding-bottom: 70%;
	padding-top: <?php echo $border; ?>;
	height: 0;
	overflow: hidden;
}
.iframe-rwd iframe {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}

.important::before {
	content: "!!! ";
	color: red;
}

.icon {
	content: "";
	font-family: FontAwesome, 'FontAwesome2';
	font-weight: normal;
	font-style: normal;
	margin: 0px 0px 0px 10px;
	text-decoration: none;
	margin-bottom: 0.5em;
	display: <?php echo $icons; ?>;
}

#univ::before {
	content: '\f19c' !important;
}

#addr::before {
	content: '\f0e0' !important;
}

#tel::before {
	content: '\f095' !important;
}

#mail::before {
	content: '@' !important;
	font-weight: bold !important;
}

/* Mobile phones: */
[class*="col-"] {
	width: 100%;
}
.col-m-0 {width: 0;}


/* Tablets: */
@media only screen and (min-width: 600px) and (max-width: 1000px) {
	* {
		animation-duration: <?php echo $anim_trans_time_t; ?> !important;
		-o-animation-duration: <?php echo $anim_trans_time_t; ?> !important;
		-moz-animation-duration: <?php echo $anim_trans_time_t; ?> !important;
		-webkit-animation-duration: <?php echo $anim_trans_time_t; ?> !important;
		-webkit-transition: all <?php echo $anim_trans_time_t; ?> !important;
		-moz-transition: all <?php echo $anim_trans_time_t; ?> !important;
		-ms-transition: all <?php echo $anim_trans_time_t; ?> !important;
		-o-transition: all <?php echo $anim_trans_time_t; ?> !important;
	}
	
	html {
		font-size: <?php echo $font_t; ?> !important;
	}

	.col-t-0 {width: 0;}
	.col-t-1 {width: 8.33%;}
	.col-t-2 {width: 16.66%;}
	.col-t-3 {width: 25%;}
	.col-t-4 {width: 33.33%;}
	.col-t-5 {width: 41.66%;}
	.col-t-6 {width: 50%; }
	.col-t-7 {width: 58.33%;}
	.col-t-8 {width: 66.66%;}
	.col-t-9 {width: 75%;}
	.col-t-10 {width: 83.33%;}
	.col-t-11 {width: 91.66%;}
	.col-t-12 {width: 100%;}
	
	.menu li {
		float: none !important;
	}
	
	.menu li:hover {
		padding-left: 8px !important;
	}

	.menu li:active {
		transform: translate(10%, 0) !important;
		-o-transform: translate(10%, 0) !important;
		-moz-transform: translate(10%, 0) !important;
		-webkit-transform: translate(10%, 0) !important;
	}
	
	.logo {
		float: right !important;
		transform: translate(0, 65%) !important;
		-o-transform: translate(0, 65%) !important;
		-moz-transform: translate(0, 65%) !important;
		-webkit-transform: translate(0, 65%) !important;
	}
	
	.col-t-r {
		float: right !important;
	}
}

/* Desktop: */
@media only screen and (min-width: 1001px) {
	* {
		animation-duration: <?php echo $anim_trans_time_d; ?> !important;
		-o-animation-duration: <?php echo $anim_trans_time_d; ?> !important;
		-moz-animation-duration: <?php echo $anim_trans_time_d; ?> !important;
		-webkit-animation-duration: <?php echo $anim_trans_time_d; ?> !important;
		-webkit-transition: all <?php echo $anim_trans_time_d; ?> !important;
		-moz-transition: all <?php echo $anim_trans_time_d; ?> !important;
		-ms-transition: all <?php echo $anim_trans_time_d; ?> !important;
		-o-transition: all <?php echo $anim_trans_time_d; ?> !important;
	}
	
	html {
		font-size: <?php echo $font_d; ?> !important;
		height: 100vh;
		max-height: 100vh;
	}
	
	.col-0 {width: 0;}
	.col-1 {width: 8.33%;}
	.col-2 {width: 16.66%;}
	.col-3 {width: 25%;}
	.col-4 {width: 33.33%;}
	.col-5 {width: 41.66%;}
	.col-6 {width: 50%;}
	.col-7 {width: 58.33%;}
	.col-8 {width: 66.66%;}
	.col-9 {width: 75%;}
	.col-10 {width: 83.33%;}
	.col-11 {width: 91.66%;}
	.col-12 {width: 100%;}
	
	.logo:hover {
		filter: brightness(120%) contrast(90%);
		transform: scale(1.15, 1.15);
		-o-transform: scale(1.15, 1.15);
		-moz-transform: scale(1.15, 1.15);
		-webkit-transform: scale(1.15, 1.15);
	}
	
	.menu li {
		float: none !important;
	}
	
	.menu li:hover {
		padding-left: <?php echo $border; ?>  !important;
	}

	.menu li:active {
		transform: translate(1.5vh, 0.7vh) scale(0.9)  !important;
		-o-transform: translate(1.5vh, 0.7vh) scale(0.9)  !important;
		-moz-transform: translate(1.5vh, 0.7vh) scale(0.9)  !important;
		-webkit-transform: translate(1.5vh, 0.7vh) scale(0.9)  !important;
		box-shadow: none;
	}
	
	#pasteContent {
		height: 100%;
	}
	
	.aside {
		height: 100%;
	}
	

	

}
